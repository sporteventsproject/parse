Scrapy==1.6.0
demjson==2.2.4
requests==2.18.4
python-dateutil==2.7.0
aiohttp==3.5.4
scrapy-splash==0.7.2
gunicorn==19.9.0