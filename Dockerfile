FROM python:3.6.8-alpine3.9

MAINTAINER Philip Bormotov <p.bormotov@brandquad.ru>

RUN apk update
RUN apk add bash \
    procps\
	gcc \
	build-base \
	linux-headers \
	libressl-dev \
	musl-dev \
	libffi-dev \
	libxml2-dev \
	libxslt-dev \
	nginx && \
	python3 -m ensurepip && \
    pip3 install --upgrade pip && \
    rm -r /root/.cache

RUN mkdir /code
WORKDIR /code
ADD requirements.txt /code/
RUN pip3 install -r requirements.txt
ADD src /code/

RUN apk del g++ build-base
RUN apk del gcc\
    linux-headers\
    build-base

ENV APP_TOKEN='222'
ENV WEBBFB_URL='http://127.0.0.1:8000'

ENV PROXY_TOKEN='6ab673138778e4719d1d44e4b1234cf39ed22fa7'
ENV PROXY_URL='https://bfbproxy.stage.brandquad.ru'

ENV SPLASH_URL='http://splash-service:8050'
ENV PYTHONUNBUFFERED=1

EXPOSE 5858

CMD ["gunicorn", "src.server_app.server:main_app", "--bind=0.0.0.0:5858", "--workers=20", "--timeout=300", "--worker-class=aiohttp.GunicornWebWorker"]