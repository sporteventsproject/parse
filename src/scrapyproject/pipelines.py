# -*- coding: utf-8 -*-



import html

from scrapy.exceptions import DropItem

from .spiders.constants._metadata import METADATA_KEYS
from .spiders.constants.hasbro_brands import HASBRO_BRANDS, HASBRO_PREFIX
from .spiders.helpers.helpers import *


class Spiders2Pipeline(object):
    def process_item(self, item, spider):
        return item


class HasbroBrandsFilter(object):
    """

    """
    def open_spider(self, spider):
        if HASBRO_PREFIX in spider.name:
            self.brand_list = HASBRO_BRANDS

    def process_item(self, item, spider):
        if HASBRO_PREFIX in spider.name:
            # find brands from list in item
            present = False
            s = str(item).upper()
            for i in self.brand_list:
                if i in s:
                    return item
            raise DropItem()
        return item


from .spiders.constants.stada_bayer_brands import *


class StadaBayerPipeline(object):

    def process_item(self, item, spider):
        try:
            item['brand'] = item['brand'].strip()
        except:
            item['brand'] = ''

        if not item['brand']:
            t = item['title'].upper()
            if t:
                t = clear_string(t)
                t = re.sub(r'\s+-\s+', '-', t)
            l = list()
            l = l + self._get_candidate(t, BRANDS['STADA'])
            l = l + self._get_candidate(t, BRANDS['BAYER'])
            l = l + self._get_candidate(t, BRANDS['UNI'])
            l = l + self._get_candidate(t, BRANDS['ADD'])
            # l = list(filter(lambda x: x.upper()  in t, BRANDS['STADA']))
            # if not l:
            #     l = list(filter(lambda x: x.upper()  in t, BRANDS['BAYER']))
            # if not l:
            #     l = list(filter(lambda x: x.upper()  in t, BRANDS['UNI']))
            if l:
                #l = list(map(lambda x:{'l': len(x), 'v':x} , l))
                l = max(l, key=lambda item: len(item))
                item['brand'] = l #",".join(l)
        return item

    def _check(self, s, ptrn):
        try:
            u = re.search(r'\b{}\b'.format(ptrn), s, re.IGNORECASE)
            return u.group()
        except:
            return None

    def _get_candidate(self, s, l=list()):
        return list(filter(lambda x: self._check(s, x), l))



class DataFormatChecker(object):
#
# Проверка форматов выходных данных осуществляется здесь,
# если какие либо данные не соответствуют, делается попытка привести их к требуемому виду
#
    def process_item(self, item, spider):
        """

        :param item:
        :param spider:
        :return:
        """
        if '_search' in spider.name:
            return item
        if '_category' in spider.name:
            return item
        if '_reviews' in spider.name:
            return item
        # title
        item['title'] = clear_string(item.get('title', 'No Title'))
        # brand
        item['brand'] = item.get('brand', '')
        return item


class BaseSpiderFormatChecker(object):

    def process_item(self, item, spider):
        """

        :param item:
        :param spider:
        :return:
        """
        l = list(filter(lambda x: x in spider.name, ['_search' '_category' '_reviews']))
        if not l:
            item = self._check_fields(item)
            self._check_title(item)
            self._check_brand(item)
            self._check_section(item)
            self._check_stock(item)
            self._check_price(item)
            self._check_images(item)
            self._check_meta(item)
            self._check_rpc(item)
        return item

    def _check_meta(self, item):
        _metadata = item.get('metadata', {})
        #
        _d = _metadata.get('__description', 'No description')
        if isinstance(_d, list):
            _d = "".join(_d)
        _d = clear_string(_d)
        _metadata['__description'] = _d
        # keysgr
        try:
            _k = list(filter(lambda x: '__description' not in x, list(item.get('metadata', {}).keys())))
        except:
            pass
        finally:
            if isinstance(_k, list):
                for i in _metadata:
                    if isinstance(_metadata[i], list):
                        _metadata[i] = "".join(_metadata[i])
                _l = list(map(lambda x: (x.upper(), clear_string(_metadata.get(x, ''))), _k))
                for keys in METADATA_KEYS:
                    keys = [x.upper() for x in keys]
                    _l = [(keys[0], x[1]) if x[0] in keys else x for x in _l]
                for i in _k:
                    _metadata.pop(i)
                _metadata.update(_l)
        item['metadata'] = _metadata

    def _check_stock(self, item):
        """

        :param item:
        :return:
        """
        stock = item.get('stock', {"in_stock": False, "count": 0})
        item['stock'] = stock

    def _check_images(self, item):
        """

        :param item:
        :return:
        """
        assets = item.get('assets', {"main_image": '', "set_images": [], "view360": [], "video": []})
        try:
            images = assets.get('set_images', [])
            images = [re.sub(r'\?.*$', '', x) if any(y in x for y in ['.jpg?', '.png?', '.jpeg?']) else x for x in images]
            assets['set_images'] = images
            if not assets['main_image']:
                assets['main_image'] = images[0]
        except IndexError:
            assets['main_image'] = ''
        item['assets'] = assets

    def _check_title(self, item):
        """

        :param item:
        :return:
        """
        title = item.get('title', 'No title')
        if not title:
            title = ''
        try:
            title = html.unescape(title)
        except:
            pass
        title = re.sub(r',\s+,\s+', ', ', title)
        title = re.sub(r',\s+$', ' ', title)
        title = clear_string(title)

        item['title'] = title
        #return item

    def _check_brand(self, item):
        """

        :param item:
        :return:
        """
        brand = item.get('brand', 'No Brand')
        brand = html.unescape(clear_string(brand))
        if not brand  or 'No Brand' in brand:
            brand = ''
        item['brand'] = brand  #
        #return item

    def _check_price(self, item):
        p = item.get('price_data', {'current': 0., 'original': 0., 'sale_tag': None})
        prices = []
        pc = p.get('current', 0)
        po = p.get('original', 0)
        if not isinstance(pc, (type(0.), type(0))):
            if isinstance(pc, type('')):
                pc = price_clear(clear_string(pc))
            else:
                pc = 0.
        prices.append(float(pc))
        if not isinstance(po, (type(0.), type(0))):
            if isinstance(po, type('')):
                po = price_clear(clear_string(po))
            else:
                po = 0.
        prices.append(float(po))
        prices = list(set(filter(lambda x: x > 0., prices)))
        if prices:
            po = max(prices)
            pc = min(prices)
        else:
            po = 0.
            pc = 0.
        d = None
        if po != pc and po > 0.:
            d = 'Скидка ' + str(round((1 - pc / po) * 100)) + '%'
        prices = {"current": pc, "original": po, "sale_tag": d}
        item['price_data'] = prices

    def _check_section(self, item):
        _drop_words = ('ГЛАВНАЯ', 'КАТАЛОГ', 'Домашняя страница'.upper(), 'НАЗАД', 'Интернет-магазин'.upper(),)
        _s = list(filter(lambda x: not list(filter(lambda y: y in x.upper(), _drop_words))
                                      , item.get('section', [])))
        _s = list(filter(lambda x: x, _s))
        item['section'] = _s
        #return item

    def _check_marketing_tag(self, item):
        """
        TODO:IMPLEMENT
        :param item:
        :return:
        """
        pass

    def _check_fields(self, item):
        FIELDS = (('timestamp', 0), ('RPC', ''), ('url', ''), ('title', ''), ('marketing_tags', []), ('brand', ''),
            ('section', []), ('price_data', {"current": 0, "original": 0, "sale_tag": None}),
            ('stock', {"in_stock": True, "count": 0}),
            ('assets', {"main_image": '', "set_images": [], "view360": [], "video": []}),
            ('metadata', {"__description": ''}),)
        for key in FIELDS:
            try:
                v = item[key[0]]
            except:
                item[key[0]] = key[1]
        return item

    def _check_rpc(self, item):
        rpc = item.get('RPC')
        if rpc in {'None', None, ''}:
            raise DropItem()
        if not isinstance(rpc, str):
            rpc = str(rpc)
        item['RPC'] = rpc.strip()

MAX = 9


class SearchSpiderLimiter(object):
    """
    принудительно отбрасываем лишние элементы
    """

    def process_item(self, item, spider):
        if '_search' in spider.name or '_category' in spider.name:
            n = item.get('page_number', MAX + 1)
            if not isinstance(n, int):
                n = int(n)
                item['page_number'] = n
            if item.get('page_number', MAX + 1) > MAX:
                raise DropItem()
        return item


class SearchSpiderFormatChecker(object):

    def process_item(self, item, spider):
        if '_search' in spider.name:
            if len(item['result']) > 0:
                item = self._check_key_url(item)
            else:
                raise DropItem()
        return item

    def _check_key_url(self, item):
        item['result'] = list(map(lambda x: {'RPC': str(x['RPC']), 'score': x['score']}, item['result']))
        return item

class CategorySpiderFormatChecker(object):

    def process_item(self, item, spider):
        if '_category' in spider.name:
            item = self._check_key_url(item)
        return item

    def _check_key_url(self, item):
        item_keys = item.keys()
        if 'search_terms' in item_keys:
            item['url'] = item.pop('search_terms')
        return item


class ReviewSpiderFormatChecker(object):
    """
    Проверяем формат данных на соответствие
    Проверка дублей  (RPC, review_id) ?
    """

    #  {'RPC': '',  # RPC товара
    #   'attachments': [],  # Array of strings, // Вложения
    #   'country': '',  # Страна
    #   'rating': 0,  # Рейтинг отзыва
    #   'review_id': '',  # str  id отзыва
    #   'text': {
    #       'title': '',  # Заголовок отзыва
    #      'comment': ''
    #       'positive': 0 int Отзыв полезен
    #       'negative': 0 int Отзыв бесполезен
    # }  # // Текст    отзыва
    # 'type': '',  # *, // Тип     review / comment
    # 'username': ''}  # // Имя    пользователя

    ftypes = (('RPC', str), ('attachments', list),
              ('country', str), ('rating', int), ('review_id', str),
              ('text', dict), ('type', str), ('username', str))

    def process_item(self, item, spider):
        if '_reviews' in spider.name:
            # rpc
            rpc = item.get('RPC', None)
            if rpc:
                if not isinstance(rpc, str):
                    item['RPC'] = str(rpc)
            else:
                raise DropItem()
            # comment
            comt = item.get('text', {}).get('comment', '')
            if comt:
                if isinstance(comt, list):
                    comt = "".join(comt)
                if not isinstance(comt, str):
                    comt = str(comt)
                item['text']['comment'] = comt
            # positive negative
            positive = item.get('text', {}).get('positive', 0)
            negative = item.get('text', {}).get('negative', 0)
            try:
                item['text'].update({'positive': int(positive), 'negative': int(negative)})
            except:
                item['text'].update({'positive': 0, 'negative': 0})
            # review_id
            review_id = item.get('review_id', None)
            if review_id:
                if not isinstance(review_id, str):
                    item['review_id'] = str(review_id)
            else:
                raise DropItem()
            # type
            t = item.get('type', 'review')
            if t not in ('review', 'comment'):
                t = 'review'
            item['type'] = t
            # rating
            t = item.get('rating', .0)
            try:
                t = float(t)
            except:
                t = .0
            item['rating'] = t
        return item
