# -*- utf-8 -*-

from .error import *
from ..helpers.helpers import *

class BaseParser(object):


    def get_next_page(self, response, xp):
        nxt = response.xpath(xp).extract_first(default=None)
        if nxt:
            return get_full_link(get_domain(response.url), nxt)
        raise DataOutException()

    def get_price(self, prices=[]):
        discount = ''
        try:
            po = min(prices)
            pc = max(prices)
        except:
            pass

        if pc != po:
            discount = 'Скидка ' + str(int((1 - pc / po) * 100)) + '%'
        return {'price_data': {'origin': po, 'current': pc, "sale_tag": discount}}

    def get_assets(self, images=[], video=[], view=[]):
        try:
            mi = images[0]
        except IndexError:
            mi = ''
        return {'assets': {'main_image': mi,
                           'set_images': images,
                           'view360': view,
                           'video': video}}

    def get_marketing_tags(self, tags=[]):
        return {'marketing_tags': tags}

    def get_stock(self, stock=True, count=0):
        return {'stock': {'in_stock': stock, 'count': count}}

    def get_metadata(self, descr='', prop=None):
        # prop = [(pname, pval), (pname1, pval2)]
        #
        o = {'__description': descr}
        try:
            o.update(prop)
        except:
            pass
        return {'metadata': o}

    def get_RPC(self, rpc=None, dup=True, err=True):
        if not rpc and err:
            raise DataOutException()
        if rpc in self.rpcs and dup:
            raise DublicateRPCException()
        self.rpcs.add(rpc)
        return {'RPC': rpc}

    def merge_dict(self, d1, d2):
        pass

    def get_RPC_List(self):
        pass

    def _to_float(self, v):
        pass

    def _to_int(self, v):
        pass

    def _to_str(self, v):
        pass

    def _to_list(self, v):
        pass

    def _to_dict(self, v):
        pass
