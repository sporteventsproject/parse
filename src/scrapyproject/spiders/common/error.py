# -*- coding: utf-8 -*-


class SpiderException(Exception):
    pass


class BadDataFormatException(SpiderException):
    # получены данные которые не соответствуют ожидаемому формату
    pass


class DataOutException(SpiderException):
    # генерируется если данные закончились
    pass


class PassedDataOutException(SpiderException):
    # если отсутствуют ожидаемые данные в meta от предидущего запроса
    pass


class NoSearchKeywordsException(SpiderException):
    pass


class DublicateRPCException(SpiderException):
    pass


class AlwayVisitedException(SpiderException):
    pass
