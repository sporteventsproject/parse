# -*- coding: utf-8 -*-

import json
import os
import re
import requests
from scrapy import Spider, Selector
from ..helpers.helpers import boolValue, get_timestamp

class Context(object):

    def __init__(self):
        pass

    @classmethod
    def create(cls):
        return cls()

class BaseSpider(Spider):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)



    def get_bet_groups_urls(self, start_urls=list(), **kwargs):
        WEB_URL = os.getenv('WEB_URL', 'http://127.0.0.1:8000/')
        url = WEB_URL + 'get-bet-groups-urls'
        if not start_urls:
            response = requests.post(url=url).json()
            start_urls = response['urls']
        return start_urls


    def get_bet_data_urls(self, start_urls=list(), **kwargs):
        # {'data': [{'ref': '', 'url': ''}]}
        WEB_URL = os.getenv('WEB_URL', 'http://127.0.0.1:8000/')
        url = WEB_URL + 'get-bet-data-urls'
        if not start_urls:
            response = requests.post(url=url).json()
            start_urls = response['data']
        return start_urls


    def closed(self, reason):

        self.logger.info("Spider {} finished".format(self.name))
        self.logger.info("Spider sleep before sent data")






