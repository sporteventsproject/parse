from ..helpers.helpers import get_absolute_link, remove_html_tags


def get_brand(json_data):
    return json_data.get('BrandLinks', {}).get('Name', 'No Brand')


def get_price_data(json_data):
    try:
        current = float(json_data['SaleBlock']['TotalPriceInfo']['PriceValue'])
    except (KeyError, ValueError):
        current = 0

    try:
        original = float(json_data['SaleBlock']['TotalPriceInfo']['DiscountInfo']['BeforeDiscountPriceValue'])
    except (KeyError, ValueError):
        original = current

    try:
        discount = json_data['SaleBlock']['TotalPriceInfo']['DiscountInfo']['Percent']
        sale_tag = f'Скидка {int(discount)} %'
    except (KeyError, ValueError):
        sale_tag = None

    return {
        'current': current,
        'original': original,
        'sale_tag': sale_tag
    }


def get_stock_data(json_data):
    try:
        in_stock = True if json_data['SaleBlock']['Plugins']['AvailabilityInfo'] else False
    except KeyError:
        in_stock = False

    return {
        'in_stock': in_stock,
        'count': None
    }


def get_assets(json_data):
    assets = {
        'main_image': None,
        'set_images': [],
        'view360': [],
        'video': []
    }

    for item in json_data.get('Gallery', {}).get('Groups', []):

        if item.get('Name', []) == 'Фотографии':
            for image in item.get('Elements', []):
                try:
                    assets['set_images'].append(get_absolute_link(image['Original']))
                    assets['main_image'] = assets['set_images'][0]
                except (IndexError, KeyError):
                    pass

        if item.get('Name', []) == 'Видео':
            for video in item.get('Elements', []):
                try:
                    url = video['LinkToMedia']
                    assets['video'].append(url if url.startswith('http') else f'https://www.youtube.com/watch?v={url}')
                except (IndexError, KeyError):
                    pass

    return assets


def get_metadata(json_data):
    metadata = {}

    try:
        metadata['__description'] = remove_html_tags(json_data['Description']['Blocks'][0]['Text'])
    except (KeyError, IndexError):
        metadata['__description'] = ''

    try:
        for item in json_data['Capabilities']['Capabilities']:
            key = item.get('Name', None)
            value = item.get('Value', {}).get('Text', None) \
                if type(item['Value']) is dict else item.get('Value', None)

            if key and value:
                if type(value) is list: continue
                metadata[key] = value.strip()
    except KeyError:
        pass

    return metadata


def get_marketing_tags(json_data):
    try:
        return [item['Name'] for item in json_data['MarketingTags']['Tags']]
    except KeyError:
        return []
