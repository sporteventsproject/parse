# -*- coding:utf-8 -*-

# spider constants template
# 28.08.2019
#
# source: oddsportal.com
#


# data for local debugging
SEARCH_TERMS = [
    #''
]
START_URLS = [
    # data parse
    # {'url': 'https://fb.oddsportal.com/feed/match/1-1-0UuL1Dml-1-3-yj92e.dat?_=1568531942673', 'ref': 'https://www.oddsportal.com/soccer/argentina/pri mera-nacional/temperley-alvarado-bHsK7dN7/'}


    # groups parse
    # "https://www.oddsportal.com/soccer/england/premier-league/chelsea-liverpool-GUPI3B4E/#1X2;2"

    # events
    # "https://www.oddsportal.com/soccer/argentina/superliga/arsenal-sarandi-union-de-santa-fe-CA8mhCuh/"

]

import json

# START_URLS = open('./events.json').read()
# START_URLS = json.loads(START_URLS)
# START_URLS = [x['url'] for x in START_URLS]


PL_GROUP_MATCH_URLS = "//div[@id='s_1']//li[./a]/a/@href"
PL_MATCH_URLS = "//div[@id='col-content']//table[contains(@class, 'table-main')]//td[contains(@class, 'table-participant')]/a/@href"

# match
PP_MATCH_NAME = "//h1/text()"
PP_MATCH_START = "//p[contains(@class, 'datet')]/@class"
PP_MATCH_ID = "//div[@id='event-status']/@xeid"
PP_MATCH_GROUP = "//div[@id='breadcrumb']//a/text()"
