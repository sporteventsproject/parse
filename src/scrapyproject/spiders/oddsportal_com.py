# -*- coding:utf-8 -*-

# template for spider
# 28.08.2019
#
# source: oddsportal.com
#
from functools import reduce
from scrapy import Request
from scrapy.selector import Selector
from copy import deepcopy
import os
from scrapy_splash import SplashRequest

from .constants.oddsportal_com import *
from .common.core import BaseSpider
from .helpers.helpers import *
from .common.error import *
import base64
import time

WEB_URL = os.getenv('WEB_URL', 'http://127.0.0.1:8000/')

class OddsportalParser():

    def value_calc(self, k1, k2, kp1, kp2):
        marg_pin = 1/(1/kp1 + 1/kp2) * 100
        val1 = (k1 * 100 / kp1 - 100) - (100 - marg_pin)
        val2 = (k2 * 100 / kp2 - 100) - (100 - marg_pin)
        return [val1, val2]

    def value_calc_3 (self, k1, k2, k3, kp1, kp2, kp3):
        marg_pin = 1/(1/kp1 + 1/kp2 + 1/kp3) * 100
        val1 = (k1 * 100 / kp1 - 100) - (100 - marg_pin)
        val2 = (k2 * 100 / kp2 - 100) - (100 - marg_pin)
        val3 = (k3 * 100 / kp3 - 100) - (100 - marg_pin)
        return [val1, val2, val3]



    def get_json(self, response):
        resp = response
        resp = re.sub(r'^.*.dat\',\s*', '', resp)
        resp = re.sub(r'\);\s*$', '', resp)
        try:
            resp = json.loads(resp)
        except JSONDecodeError:
            pass
        return resp

    def get_over_under_values(self, jo):
        result = []
        try:
            jo = jo['d']['oddsdata']['back']
            if not jo:
                return []
            for id_bet, data_bet in jo.items():
                data_bet = data_bet['odds']
                # if data has pinnackle bk
                if data_bet.get('18'):
                    pin_bets = data_bet.get('18')
                    if isinstance(pin_bets, dict):
                        pin_bets = list(pin_bets.values())
                    for bk_id, bk_bets in data_bet.items():
                        if isinstance(bk_bets, dict):
                            bk_bets = list(bk_bets.values())
                        if len(bk_bets) > 1 and len(pin_bets) > 1:
                            values = self.value_calc(bk_bets[0], bk_bets[1], pin_bets [0], pin_bets[1])
                            if values[0] > 0:
                                r = {
                                    'bet_sub_group': id_bet,
                                    'bk': bk_id,
                                    'k_bk': bk_bets[0],
                                    'k_ref': pin_bets [0],
                                    'value': values[0]}
                                result.append(r)
                            if values[1] > 0:
                                r = {
                                    'bet_sub_group': id_bet,
                                    'bk': bk_id,
                                    'k_bk': bk_bets[1],
                                    'k_ref': pin_bets[1],
                                    'value': values[1]}
                                result.append(r)
        except KeyError:
            self.log('Refresh Page Error')
        return result

    def get_1x2_values(self, jo):
        result = []
        try:
            jo = jo['d']['oddsdata']['back']
            if not jo:
                return []
            for id_bet, data_bet in jo.items():
                data_bet = data_bet['odds']
                # if data has pinnackle bk
                if data_bet.get('18'):
                    pin_bets = data_bet.get('18')
                    if isinstance(pin_bets, dict):
                        pin_bets = list(pin_bets.values())
                    for bk_id, bk_bets in data_bet.items():
                        if isinstance(bk_bets, dict):
                            bk_bets = list(bk_bets.values())
                        if len(bk_bets) > 1 and len(pin_bets) > 1:
                            values = self.value_calc_3(bk_bets[0], bk_bets[1], bk_bets[2],
                                                     pin_bets[0], pin_bets[1], pin_bets[2])
                            if values[0] > 0:
                                r = {
                                    'bet_sub_group': id_bet,
                                    'bk': bk_id,
                                    'k_bk': bk_bets[0],
                                    'k_ref': pin_bets[0],
                                    'value': values[0]}
                                result.append(r)
                            if values[1] > 0:
                                r = {
                                    'bet_sub_group': id_bet,
                                    'bk': bk_id,
                                    'k_bk': bk_bets[1],
                                    'k_ref': pin_bets[1],
                                    'value': values[1]}
                                result.append(r)
                            if values[2] > 0:
                                r = {
                                    'bet_sub_group': id_bet,
                                    'bk': bk_id,
                                    'k_bk': bk_bets[2],
                                    'k_ref': pin_bets[2],
                                    'value': values[2]}
                                result.append(r)
        except KeyError:
            self.log('Refresh Page Error')
        return result

    def get_ah_values(self, jo):
        return []

class OddsportalEventsSpider(BaseSpider, OddsportalParser):
    name = "oddsportal.com_events"
    start_urls = ['https://www.oddsportal.com']
    custom_settings = {
        'DOWNLOAD_TIMEOUT': 180,
        'CONCURRENT_REQUESTS': 4,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 4,
        'COOKIE_ENABLED': True,
        'DOWNLOAD_DELAY': .1,
        'AUTOTHROTTLE_TARGET_CONCURRENCY': 10,
        'DOWNLOADER_MIDDLEWARES': {
            'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': None,
            'scrapyproject.extensions.random_useragent.RandomUserAgent': 400,
        },
        'BFB2_UPLOAD': True,
        'BFB2_ITEM_COUNT': 10,
        'BFB2_UPLOAD_URL': WEB_URL + 'upload-events',
        'EXTENSIONS': {
            'scrapyproject.extensions.ext_upload_result.UploadResult2BFB': 501,
        }
    }


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def parse(self, response):
        group_match_urls = response.xpath(PL_GROUP_MATCH_URLS).extract()
        for url in group_match_urls:
            url = response.urljoin(url)
            yield Request(url=url, callback=self.get_match_urls)

    def get_match_urls(self, response):
        match_urls = response.xpath(PL_MATCH_URLS).extract()
        for url in match_urls:
            url = response.urljoin(url)
            yield Request(url=url, callback=self.get_match_info)

    def get_match_info(self, response):
        name = response.xpath(PP_MATCH_NAME).extract_first(default='')
        data_start = self.parse_date(response)
        id = response.xpath(PP_MATCH_ID).extract_first(default='')
        group = response.xpath(PP_MATCH_GROUP).extract()
        group = [x for x in group if 'Home' not in x]
        sport_type = group[0] if group else ''
        yield {
            'id': id,
            'name': name,
            'url': response.url,
            'data_start': data_start,
            'group': '>>'.join(group),
            'sport_type': sport_type
        }

    def parse_date(self, response):
        d = response.xpath(PP_MATCH_START).extract_first(default='')
        d = re.search(r't(\d{10})', d)
        d = d.group(1) if d else 0
        return d


class OddsportalGetGroupDataSpider(BaseSpider, OddsportalParser):
    name = "oddsportal.com_groups"
    custom_settings = {
        'DOWNLOAD_TIMEOUT': 180,
        # RETRY_TIMES - 100
        # нужно для перезапросов контейнера splash после его перезапуска, иначе будут пропущенные items
        'RETRY_TIMES': 100,
        'CONCURRENT_REQUESTS': 4,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 4,
        'COOKIE_ENABLED': True,
        'DOWNLOAD_DELAY': .1,
        'AUTOTHROTTLE_TARGET_CONCURRENCY': 10,
        'DOWNLOADER_MIDDLEWARES': {
            'scrapyproject.extensions.mid_get_json.GetJSON': 200,
            'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': None,
            'scrapyproject.extensions.random_useragent.RandomUserAgent': 400,
            'scrapy_splash.SplashCookiesMiddleware': 723,
            'scrapy_splash.SplashMiddleware': 725,
            'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware': 810,
        },
        'ROBOTSTXT_OBEY': False,
        'SPLASH_URL': 'http://0.0.0.0:8050',
        'SPIDER_MIDDLEWARES': {
            'scrapy_splash.SplashDeduplicateArgsMiddleware': 100,
        },
        'DUPEFILTER_CLASS': 'scrapy_splash.SplashAwareDupeFilter',
        'BFB2_UPLOAD': True,
        'BFB2_ITEM_COUNT': 10,
        'BFB2_UPLOAD_URL': WEB_URL + 'upload-bet-values',
        'EXTENSIONS': {
            'scrapyproject.extensions.ext_upload_result.UploadResult2BFB': 501,
        }
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.start_urls = self.get_bet_groups_urls(START_URLS)

    def start_requests(self):
        for url_gr in self.start_urls:
                yield SplashRequest(url=url_gr, callback=self.parse_har, endpoint='render.har',
                                    args={
                                        'html': 1,
                                        'images': 0,
                                        'html5_media': 0,
                                        'wait': 7,
                                        'request_body': 1,
                                        'response_body': 1
                                    },
                                    dont_filter=True)

    def parse_har(self, response):
        jo = json.loads(response.text)
        jo = jo['log']['entries']
        jo = [x for x in jo if '/match/' in x.get('request', {}).get('url', '')]
        if jo:
            # event id
            event_id = re.search(r'/\d-\d-(\w+)-', jo[0].get('request', {}).get('url', '')).group(1)
            # group id
            group_id = re.search(r'/\d-\d-\w+-(\d-\d)', jo[0].get('request', {}).get('url', '')).group(1)
            # url_data
            url_data = jo[0]['request']['url']
            # data
            resp = jo[0].get('response',{}).get('content', {}).get('text', '') if jo else ''
            resp = base64.standard_b64decode(resp).decode("utf-8")
            jo = self.get_json(resp)
            # get values
            # Over-Under
            if group_id in ['2-2', '2-3', '2-4']:
                bet_values = self.get_over_under_values(jo)
            # Asian Handicap
            elif group_id in ['5-2', '5-3', '5-4']:
                bet_values = self.get_over_under_values(jo)
                # bet_values = self.get_ah_values(jo)
            # 1X2
            elif group_id in ['1-2', '1-3', '1-4']:
                bet_values = self.get_1x2_values(jo)
            else:
                bet_values = []
            yield {'event_id': event_id, 'group_id': group_id, 'url_data': url_data, 'bet_values': bet_values}




class OddsportalGetDataSpider(BaseSpider, OddsportalParser):
    name = "oddsportal.com_data"
    custom_settings = {
        'DOWNLOAD_TIMEOUT': 180,
        'CONCURRENT_REQUESTS': 4,
        'CONCURRENT_REQUESTS_PER_DOMAIN': 4,
        'COOKIE_ENABLED': True,
        'DOWNLOAD_DELAY': .1,
        'AUTOTHROTTLE_TARGET_CONCURRENCY': 10,
        'DOWNLOADER_MIDDLEWARES': {
            'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': None,
            'scrapyproject.extensions.random_useragent.RandomUserAgent': 400,
        },
        'BFB2_UPLOAD': True,
        'BFB2_ITEM_COUNT': 1000,
        'BFB2_UPLOAD_URL': WEB_URL + 'upload-bet-values',
        'EXTENSIONS': {
            'scrapyproject.extensions.ext_upload_result.UploadResult2BFB': 501,
        }
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.start_urls = self.get_bet_data_urls(START_URLS)

    def start_requests(self):
        for url_ref in self.start_urls:
            if not url_ref['url_data']:
                continue
            t = time.time()
            t = str(t).replace('.', '')[:13]
            url = re.sub(r'\d+$', t, url_ref['url_data'])
            headers = {'Accept': '*/*', 'Referer': url_ref['url']}
            yield Request(url=url, callback=self.parse, headers=headers)

    def parse(self, response):
        jo = self.get_json(response.text)
        if jo:
            # event id
            event_id = re.search(r'/\d-\d-(\w+)', response.url).group(1)
            # group id
            group_id = re.search(r'/\d-\d-\w+-(\d-\d)', response.url).group(1)

            # get values
            if group_id in ['2-2', '2-3', '2-4']:
                bet_values = self.get_over_under_values(jo)
            elif group_id in ['5-2', '5-3', '5-4']:
                bet_values = self.get_ah_values(jo)
            elif group_id in ['1-2', '1-3', '1-4']:
                bet_values = self.get_1x2_values(jo)
            else:
                bet_values = []
            yield {'event_id': event_id, 'group_id': group_id, 'url_data': response.url, 'bet_values': bet_values}


