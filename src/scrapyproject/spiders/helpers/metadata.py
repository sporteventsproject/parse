# -*- coding:utf-8 -*-
#
# if you do not know or do not understand how it works - do not go
# s.deynego@brandquad.ru
#
import re

from .helpers import remove_html_tags, clear_string


class MetadataUtils(object):

    @staticmethod
    def extract_sub(response, xpth, subheader=[], sep=[]):
        result = []
        return result

    @staticmethod
    def get_property(response, xpth, tag=r'<strong>', sep=':', deprecated_keys=()):
        """

        :param response:
        :param xpth: xpath expression
        :param tag: html tag around KEY
        :param sep:
        :param deprecated_keys: keys excluded from result. must be UPPER case
        :return: LIST [(key, val), (key2: val2), (keyN, valN)]
        """
        rc = re.compile(tag)
        l = list(filter(lambda y: y,
                        map(lambda x: remove_html_tags(x),
                            rc.split(response.xpath(xpth).extract_first(default=[])))))
        l = list(filter(lambda y: len(y) == 2,
                        map(lambda x: x.split(sep, 1), l)))
        l = list(filter(lambda y: y[0].upper() not in deprecated_keys,
                        map(lambda x: (x[0], clear_string(x[1]),), l)))
        return l

    @staticmethod
    def rename_key(src, keys, merge=True):
        """
        TODO: implement
        :param src: list of tuple
        :param keys: [(srck1, destk1), (), (), ...]
        :param merge: if True merge
        :return: list of tuple
        """
        m = {}
        try:
            m.update(keys)
            l = list(map(lambda x: (m.get(x[0], x[0]), x[1]), src))
            return l
        except:
            return []

    @staticmethod
    def clear_keys(src, rge=r'(\s\s+|:)'):
        return list(map(lambda x: (clear_string(re.sub(rge, '', x[0])), x[1]), filter(lambda y: y[0] and y[1], src)))

    @staticmethod
    def get_props_from_html(response, xpath_html, sep_1=r'<strong[^>] >', sep_2='</li>'):
        """
        разбивает html блок на параметры для metadata
        :param response:
        :param xpath_html: string xpath для получения xpath_html блока
        :param d_1: RegExp паттерн для получения списка пар ['ключ1 значение1', 'ключ2 значение2' ...]
        :param d_2: string строка делитель для полчения из 'ключ1 значение1' картежа (ключ1, значение1)
        :return: list of tuple [(ключ1, значение1), (ключ2, значение2) ...]
        """
        html_tile = response.xpath(xpath_html).extract_first(default='')
        prop = re.split(sep_1, html_tile)
        if prop:
            prop = list(map(lambda x: [x[:x.find(sep_2)], x[x.find(sep_2):]] if x.find(sep_2) != -1 else [x], prop))
            prop = list(filter(lambda x: len(x) == 2, prop))
            prop = list(map(lambda x: (clear_string(x[0]).replace(':', ''),
                                       clear_string(x[1])), prop))
        return prop

    @staticmethod
    def get_props_from_lists(response, xpath_keys, xpath_values):
        """
        :param response:
        :param xpath_keys: xpath ключей для метадаты [ключ1, ключ2, ...]
        :param xpath_values: xpath значений для метадаты [ значение1, значение2 ...]
        :return: list of tuple [(ключ1, значение1), (ключ2, значение2) ...]
        """
        prop_key = response.xpath(xpath_keys).extract()
        prop_val = response.xpath(xpath_values).extract()
        prop_val = [clear_string(x) for x in prop_val]
        prop_key = [clear_string(x).replace(':', '') for x in prop_key]
        prop = list(zip(prop_key, prop_val))
        if len(prop) != max(len(prop_key), len(prop_val)):
            prop = []
        return prop
