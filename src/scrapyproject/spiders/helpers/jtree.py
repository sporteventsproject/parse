# -*- coding:utf-8 -*-
#
# if you do not know or do not understand how it works - do not go
# s.deynego@brandquad.ru
#

from .helpers import clear_string, remove_html_tags


class JTree(object):

    def __init__(self, o={}):
        if not isinstance(o, (dict, list, tuple)):
            raise ValueError()
        self._o = o
        self._s = []

    def get_k(self, k):
        return self._find(self._o, k)

    def _find(self, o, name):
        s = ""
        if isinstance(o, dict):
            for k in o:
                if isinstance(o[k], dict):
                    s = s + "{}: ( {} ) ".format(k, self._find(o[k], name))
                    continue
                if isinstance(o[k], list):
                    s = s + "{}: [ {} ] ".format(k, self._find(o[k], name))
                    continue
                if isinstance(o[k], tuple):
                    s = s + "{}: ( {} ) ".format(k, self._find(o[k], name))
                    continue
                s = s + " {} ".format(k)
        if isinstance(o, (list, tuple)):
            for k, i in enumerate(o):
                if isinstance(i, (dict, list, tuple)):
                    s = s + "{}: ({}) ".format(k, self._find(i, name))
                    continue
                s = s + " {}: () ".format(k)
        return s

    def _get_keys(self, o):
        try:
            return list(o.keys())
        except:
            return []


class JDict(object):
    """
    work with json data
    """

    def __init__(self, o):
        self._o = o

    def key(self, key):
        return self.__getattr__(key)

    def __getattr__(self, key):
        if key != 'key':
            if isinstance(self._o, (dict, list, tuple)):
                return self._find(self._o, key)
        else:
            return self.key
        return []

    def _find(self, obj, key):
        r = list()
        if isinstance(obj, dict):
            r.append(obj.get(key, None))
            for i in list(obj.keys()):
                r.extend(self._find(obj.get(i), key))
        if isinstance(obj, (list, tuple)):
            for i in obj:
                r.extend(self._find(i, key))
        r = list(filter(lambda x: x, r))
        return r


def dict_map(map_dict, src_dict):
    res_dict = dict()
    if isinstance(src_dict, dict):
        keys = list(map_dict.keys())
        res_dict.update(list(filter(lambda y: y[0] and y[1],
                                    [(map_dict.get(x), clear_string(remove_html_tags(src_dict.get(x, '')))) for x in
                                     keys])))
    return res_dict


class JUtils(object):

    def __init__(self):
        pass

    @staticmethod
    def get_path(o, path, default=None):
        pass

    @staticmethod
    def get_item_byKeyEq(key, val, lst):
        """
        вытащить из списка объекты с заданным ключом и его значением
        :param key:
        :param val:
        :param lst:
        :return: []
        """
        return list(filter(lambda x: x.get(key, None) == val, lst))

    @staticmethod
    def get_item_byKeyNotEq(key, val, lst):
        """

        :param key:
        :param val:
        :param lst:
        :return:
        """
        return list(filter(lambda x: x.get(key, None) != val, lst))

    @staticmethod
    def get_item_byKeyPresent(key, lst):
        """

        :param key:
        :param l:
        :return:
        """
        return list(filter(lambda x: key in list(x.keys()), lst))

    @staticmethod
    def get_item_byKeyNotPresent(key, lst):
        """

        :param key:
        :param l:
        :return:
        """
        return list(filter(lambda x: key not in list(x.keys()), lst))

    @staticmethod
    def remove_items(src, remove):
        """
        возвращает список в котором отсутствуют элементы находящиеся в списке
        remove
        :param src:
        :param remove:
        :return:
        """
        s = src
        if not isinstance(src, (list, tuple)):
            s = [src]
        r = remove
        if not isinstance(remove, (list, tuple)):
            r = [remove]
        return list(filter(lambda x: x not in r, s))

    @staticmethod
    def map_to(jsrc, keys):
        """
        получить из json объекта новый объект
        :param jsrc: json
        :param keys: список соответствия ключей типа [(key_old , key_new), ()]
        key_old -> 'key1.key2.key3', key_new -> 'k1.k2'
        :return: dict
        """
        pass
