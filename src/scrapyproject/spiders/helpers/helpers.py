import re
import urllib
from datetime import datetime, timedelta
from time import mktime
from time import strptime
from urllib.parse import urlparse, urlsplit, parse_qs

from demjson import decode as dem_decode, JSONDecodeError

from ..common.error import DataOutException

MAIN_SETTINGS = {
    'ITEM_PIPELINES':
        {'scrapyproject.extensions.pipelines.DropDublicateRPC': 100,
         'scrapyproject.pipelines.BaseSpiderFormatChecker': 201}
}

CATEGORY_SETTINGS = {
    'ITEM_PIPELINES': {
            'scrapyproject.pipelines.CategorySpiderFormatChecker': 200,
        }
}

REVIEW_SETTINGS = {
    'ITEM_PIPELINES': {
        'scrapyproject.extensions.pipelines.DropDublicateComposite': 100,
        'scrapyproject.pipelines.ReviewSpiderFormatChecker': 200,
    },
    'COMPOSITE_KEYS': ['RPC', 'review_id'],
}



def get_absolute_link(url):
    try:
        if url.startswith("//"):
            return "http:{}".format(url)
    except AttributeError:
        return None


def get_full_link(domain, url):
    if re.search(r'^http(s){0,1}://', url.strip()) is not None:
        return url
    if domain[-1:] == '/':
        if url[:1] == "/":
            url = url[1:]
    else:
        if url[:1] != "/":
            url = "/" + url
    return f'{domain}{url}'


def get_domain(url):
    o = urlparse(url)
    return f'{o.scheme}://{o.netloc}'


def clear_link(url):
    return re.sub(r"\?.+$", '', url)


def get_timestamp(dt=None):
    if not dt:
        dt = datetime.now().timetuple()
    return int(mktime(dt))


def parse_price(price):
    try:
        price = price.strip().lower().replace(',', '.')
        for item in ['руб.', 'руб', 'р.', 'р', 'от', '\xa0', '&nbsp', '&#160', ' ', '(', ')', '*']:
            price = price.replace(item, '')
        if '.' not in price:
            price = price + '.0'
        return float(price)
    except (AttributeError, TypeError):
        return None


def price_clear(price_str):
    if isinstance(price_str, type('')):
        price_str = price_str.replace(' ', '')
        r = re.search(r'^\d{0,8}(\.{0,1}\d{0,2})?', price_str)
        try:
            r = r.group(0)
            if '.' not in r:
                r = r + '.0'
            return float(r)
        except:
            r = 0.
        return r
    if isinstance(price_str, type(0)):
        return price_str + .0
    return price_str


def clear_string(s):
    if isinstance(s, type(None)):
        return ''
    if isinstance(s, str):
        return remove_multiple_whitespace(remove_html_tags(s)).strip()
    return s


def get_json_from_response(reg, response):
    data = re.search(reg, response.body.decode(response.encoding).encode('utf-8'))
    #
    # if len(data.groups()) == 0:
    #    return
    try:
        return dem_decode(data.groups()[0])
    except (JSONDecodeError, AttributeError) as err:
        print(f'Error converting to JSON. \n{err} \n{response.url}')
        print(response.meta.get('redirect_urls', []))
        return {}


def get_json_object(response):
    txt = response.body.decode(response.encoding)
    try:
        return dem_decode(txt)
    except JSONDecodeError as err:
        print(f'Error converting to JSON. \n{err}')
        return None


def remove_html_tags(text):
    return re.sub(r'\<[^>]*\>', '', str(text))


def remove_multiple_whitespace(text):
    return re.sub(r'\s+', ' ', str(text).strip())


def boolValue(val):
    if type(val) is str:
        if val in ('False', 'FALSE', 'false', 'No', 'no', '',):
            return False
        return True
    return val


def remove_script(txt):
    # удалить скрипты из страницы
    s = re.sub(r'<script(>){0,1}\w+</script>', '', text)
    return remove_conrol_chars(s)


def remove_conrol_chars(s):
    # удалить управляющие символы из текста
    return re.sub(r'[\x00-\x1f\x7f-\x9f]', '', s)


def merge_dict(d1, d2):
    #
    # объединить рекурсивно словари
    #
    for k in d2:
        if k in d1 and isinstance(d1[k], dict) and isinstance(d2[k], dict):
            merge_dict(d1[k], d2[k])
        else:
            d1[k] = d2[k]


def parse_date(post_time):
    MONTH_NAMES = [
        ("janvier", "january", "янв", "январь", "января"),
        ("février", "fevrier", "february", "фев", "февраль", "февраля"),
        ("mars", "march", "мар", "март", "марта"),
        ("avril", "april", "апр", "апрель", "апреля"),
        ("mai", "may", "май", "мая"),
        ("juin", "june", "июн", "июнь", "июня"),
        ("juillet", "july", "июл", "июль", "июля"),
        ("août", "aout", "august", "авг", "август", "августа"),
        ("septembre", "september", "сен", "сент", "сентябрь", "сентября"),
        ("octobre", "october", "окт", "октябрь", "октября"),
        ("novembre", "november", "ноя", "ноябрь", "ноября"),
        ("décembre", "decembre", "december", "дек", "декабрь", "декабря")
    ]
    post_time = post_time.lower()
    if 'сегодня' in post_time:
        post_time = datetime.now().strftime('%d %m %Y')
    if 'вчера' in post_time:
        post_time = (datetime.now() - timedelta(days=1)).strftime('%d %m %Y')
    else:
        try:
            for i, month in enumerate(MONTH_NAMES, 1):
                for mon in month:
                    if mon in post_time:
                        # print('post_time: ', post_time, ' month: ', i)
                        post_time = post_time.split(" ")
                        post_time[1] = str(i)
                        post_time = ' '.join(post_time)
                        raise DataOutException
        except DataOutException:
            pass
    return strptime(post_time, '%d %m %Y')


def url_params2dict(url):
    """
    get dict from url params
    """
    try:
        d = parse_qs(urlsplit(url).query)
        for k in list(d.keys()):
            try:
                if isinstance(d[k], list):
                    d[k] = d[k][0]
            except:
                pass
        return d
    except:
        return {}


def url_dict2params(d):
    """
    Получаем из словаря строку зараметров в формате урл запроса
    значения None --> ''
    :param d: dict()
    :return: str
    """
    try:
        d.update(zip(list(d.keys()), map(lambda x: x or '', list(d.values()))))
        return urllib.parse.urlencode(d)
    except:
        return ''


def list_list2group(iterable, count=2):
    """
    разбить список на группы по count элементов
    :param iterable:
    :param count:
    :return:
    """
    return zip(*[iter(iterable)] * count)

def get_item_props(html_tile, d_1, d_2):
    """
    разбивает html блок на параметры для metadata
    :param html_tile: string
    :param d_1: string
    :param d_2: string
    :return: dict
    """
    res = {}
    prop = re.split(d_1, html_tile)
    if prop:
        prop = list(map(lambda x: [x[:x.find(d_2)], x[x.find(d_2):]] if x.find(d_2) != -1 else [x], prop))
        prop = list(filter(lambda x: len(x) == 2, prop))
        prop = list(map(lambda x: (remove_html_tags(clear_string(x[0])).upper().replace(':', ''),
                                   remove_html_tags(clear_string(x[1]))), prop))
        res.update(prop)
    return res