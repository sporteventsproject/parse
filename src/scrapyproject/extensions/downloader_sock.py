# -*- coding:utf-8 -*-

# if you do not know or do not understand how it works - do not go
# © s.deynego@brandquad.ru
#

class Proxy(object):
    """
    {"host":"",
    "port":"",
    "tip":"",
    "code":"",
    "country":""
    }

    """

    def __init__(self):
        pass


class ProxyManager(object):
    """
    TODO: Implement
    """
    def __init__(self, startport, ports):
        raise Exception("Not Implemented")
        self._ports = set()
        self._banned = list()

    def update(self, proxy_list):
        pass

    def get_proxy(self):
        pass

    def release_proxy(self, proxy):
        pass

    def _start_proxy(self, proxy, localport):
        pass

    def _release_proxy(self, localport):
        pass


class SockBridgeMiddleware(object):
    """
    TODO: Implement
    TODO: Test


    must be installed DeleGate http-socks server.

    request.meta = {'sock_bridge':{ 'multi_key': '', #this key set in response on first request
                'req_type':'single|multi|final'}

    examle use:

    SOCK_BRIDGE_LOCAL_PORT_BEGIN: 40000, # start local port for http
    SOCK_BRIDGE_LOCAL_PORT_COUNT: 50, # allocated active proxy instances
    SOCK_BRIDGE_REQUESTS_COUNT: 5, # default request count per proxy
    SOCK_BRIDGE_REUSE: True, # reuse socks proxy
    SOCK_BRIDGE_SOCKS_LIST: '', # source for list
    DOWNLOADER_MIDDLEWARES = {
        'scrapyproject.extensions.downloader_sock.SockBridgeMiddleware': 543,
    }

    """

    def __init__(self, settings):
        pass

    @classmethod
    def from_crawler(cls, spider):
        pass

    def process_request(self, request, spider):
        pass

    def process_response(self, request, response, spider):
        pass

    def process_exception(self, request, exception, spider):
        pass
