# -*- coding:utf-8 -*-
# if you do not know or do not understand how it works - do not go
#
# © s.deynego@brandquad.ru
#

from scrapy.exceptions import DropItem


class DropDublicateRPC(object):
    """
    'DROP_DUBLICATE' : True, #
    'PIPELINES':{
     'scrapyproject.extensions.pipelines.DropDublicateRPC': 100,
    }
    """

    def __init__(self, enabled=True):
        self.viewed_rpc = set()
        self.viewed_rpc.add('')
        self.viewed_rpc.add(None)
        self._enabled = enabled

    @classmethod
    def from_crawler(cls, crawler):
        return cls(enabled=crawler.settings.get('DROP_DUBLICATE', True))

    def process_item(self, item, spider):
        """

        :type item: object
        """
        if self._enabled:
            if item.get('RPC', None) in self.viewed_rpc:
                spider.logger.info("Drop dublicate iten {} {}".format(item['RPC'], item['url']))
                raise DropItem()
            self.viewed_rpc.add(item['RPC'])
        return item


class DropDublicateComposite(object):
    """
    {
    'COMPOSITE_KEYS' : [key1, key2],
    'PIPELINES':{
     'scrapyproject.extensions.pipelines.DropDublicateComposite': 100,
    }
    }

    """

    def __init__(self):
        self._values = set()

    def process_item(self, item, spider):
        keys = spider.custom_settings.get('COMPOSITE_KEYS', False)
        if keys:
            v = "_:_".join(list(map(lambda x: str(item.get(x, '')), keys)))
            if v in self._values:
                raise DropItem()
            self._values.add(v)
        return item


class MattelKeyUpdater(object):
    """
    Заменяем ключи из списка на заданный
    """
    KEY_CHANGE = {'ВОЗРАСТ': ['ВОЗРАСТ РЕБЕНКА', 'РЕКОМЕНДУЕМЫЙ ВОЗРАСТ', 'ВОЗРАСТНАЯ ГРУППА', 'ВОЗРАСТНАЯ КАТЕГОРИЯ']}

    def process_item(self, item, spider):
        keys = set(item.get('metadata', {}).keys())
        if 'ВОЗРАСТ' not in keys:
            for k in self.KEY_CHANGE.items():
                kr = list(set(k[1]).intersection(keys))
                kr = list(filter(lambda x: x, map(lambda x: item['metadata'].pop(x, 'None'), kr)))
                item['metadata'][k[0]] = "".join(kr)
        return item


class LACDLUCP_Update(object):
    """
    SPIDER-1069
    """
    SPIDERS = (("6030000.ru", "СОСТАВ"),
               ("apteka.ru", "СОСТАВ"),
               # ("goldapple.ru", ""),
               # ("iledebeaute.ru", ""),
               ("lamoda.ru", ""),
               ("letu.ru", ""),
               ("okeydostavka.ru", ""),
               ("onlinetrade.ru", ""),
               ("ozon.ru", ""),
               ("pharmacosmetica.ru", ""),
               ("podrygka.ru", ""),
               ("pudra.ru", ""),
               ("randewoo.ru", ""),
               ("rigla.ru", ""),
               ("rivegauche.ru", ""),
               ("r-ulybka.ru", ""),
               ("samson-pharma.ru", ""),
               ("utkonos.ru", ""),
               ("wildberries.ru", ""),
               ("zhivika.ru", ""),
               )
    KEY_CHANGE = {'СОСТАВ': ['_']}

    def process_item(self, item, spider):
        keys = set(item.get('metadata', {}).keys())
        if 'СОСТАВ' not in keys:
            for k in self.KEY_CHANGE.items():
                kr = list(set(k[1]).intersection(keys))
                kr = list(filter(lambda x: x, map(lambda x: item['metadata'].pop(x, 'None'), kr)))
                item['metadata'][k[0]] = "".join(kr)
        return item
