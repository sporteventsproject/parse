# -*- coding:utf-8 -*-


# if you do not know or do not understand how it works - do not go
# © s.deynego@brandquad.ru
#

import json
import logging
import time
from copy import deepcopy

import requests
from scrapy import signals
from scrapy.exceptions import NotConfigured

_HEADERS = [('Authorization', 'I{2K=5^f+D.<_{O{3Lk!x%d0;[f*(t1~M([*#o6Pd3@k@y#wo%wa~4gBgnV](kj7')]

logger = logging.getLogger(__name__)

class UploadResult2BFB(object):
    """
    Расширение для загрузки полученных данных на сервер BFB

    custom_settings = {
        'BFB2_UPLOAD': True, # инициировать отправку данных на сервер BFB
        'BFB2_ITEM_COUNT': 1000, # количество элементов в одной отправке
        'BFB2_UPLOAD_URL', # ссылка на ресурс
        EXTENSIONS = {
            'scrapyproject.extensions.ext_upload_result.UploadResult2BFB': 501,
        }

    }
    """

    def __init__(self, url, count=1000):
        self.count = count
        self.url = url
        self._items_scraped = 0
        self.job_id = str(100000 * time.time()).replace('.', '')

    @classmethod
    def from_crawler(cls, crawler):
        # first check if the extension should be enabled and raise
        # NotConfigured otherwise
        if not crawler.settings.getbool('BFB2_UPLOAD'):
            raise NotConfigured

        # get the number of items from settings
        item_count = crawler.settings.getint('BFB2_ITEM_COUNT', 1000)
        dest_url = crawler.settings.get('BFB2_UPLOAD_URL', None)
        if not dest_url:
            raise NotConfigured

        # instantiate the extension object
        ext = cls(dest_url, item_count)

        # connect the extension object to signals
        crawler.signals.connect(ext.spider_opened, signal=signals.spider_opened)
        crawler.signals.connect(ext.spider_closed, signal=signals.spider_closed)
        crawler.signals.connect(ext.item_scraped, signal=signals.item_scraped)

        # return the extension object
        return ext

    def spider_opened(self, spider):
        logger.info("opened spider %s", spider.name)
        self._items = list()
        self._part = 0

    def spider_closed(self, spider):
        logger.info("closed spider %s", spider.name)
        if len(self._items) > 0:
            self._send_file(self._items, spider)

    def item_scraped(self, item, spider):
        self._items.append(deepcopy(item))
        if len(self._items) >= self.count:
            self._send_file(self._items, spider)

    def _send_file(self, content=None, spider=None):

        try:
            logger.info("try send scraped data to BFB server")
            r = requests.post(url=self.url, data=json.dumps(content))
            if r.status_code == 200:
                logger.info("data sent successful")
                self._items = list()
                self._part = self._part + 1
            else:
                logger.error("data send error: status_code={}".format(r.status_code))
        except json.JSONDecodeError as err:
            logger.error("Error encode items to json")
