#!/usr/bin/python
#-*-coding:utf-8-*-

import random

from scrapy.downloadermiddlewares.useragent import UserAgentMiddleware

from scrapyproject.spiders.helpers.user_agents import USER_AGENTS, FIREFOX_UA, CHROME_UA


class RandomUserAgent(UserAgentMiddleware):
    """
    custom_settings = {
        'DOWNLOADER_MIDDLEWARES': {
            'scrapyproject.extensions.random_useragent.RandomUserAgent': 400,
        },
    }
    """

    def process_request(self, request, spider):
        if 'USER-AGENT' not in list(map(lambda x: x.decode('utf-8').upper(),
                                        list(request.headers.keys()))):
            ua = random.choice(USER_AGENTS)
            if spider.custom_settings.get("FIREFOX_UA_ONLY", False):
                ua = random.choice(FIREFOX_UA)
            if spider.custom_settings.get("CHROME_NEW_UA", False):
                ua = random.choice(CHROME_UA)
            if ua:
                request.headers.setdefault('User-Agent', ua)
            else:
                request.headers.setdefault('User-Agent', '')

        # Print useragent in log
        # spider.log(
        #     u'User-Agent: {} {}'.format(request.headers.get('User-Agent'), request),
        #     level=log.DEBUG
        # )

