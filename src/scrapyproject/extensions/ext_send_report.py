# -*- coding:utf-8 -*-
import logging
import time

from scrapy import signals
from scrapy.exceptions import NotConfigured

logger = logging.getLogger(__name__)


class SendReportData(object):
    """
    Расширение для отправки логов и дампа собранных данных

    custom_settings = {
        'BQ_SEND_RESULT': True, # инициировать отправку данных на сервер BFB
        'BQ_RECEIVERS': [], # список адресов почты на которые отправлять данные
        'BQ_MAIL_SERVER': '', # сервер
        'BQ_MAIL_USER':'',
        'BQ_MAIL_PASS':'',
        EXTENSIONS = {
            'scrapyproject.extensions.ext_send_report.SendReportData': 201,
        }

    }
    """

    def __init__(self, url, count=1000):
        self.count = count
        self.url = url
        self._items_scraped = 0
        self.job_id = str(100000 * time.time()).replace('.', '')

    @classmethod
    def from_crawler(cls, crawler):
        # first check if the extension should be enabled and raise
        # NotConfigured otherwise
        if not crawler.settings.getbool('BFB2_UPLOAD'):
            raise NotConfigured

        # get the number of items from settings
        item_count = crawler.settings.getint('BFB2_ITEM_COUNT', 1000)
        dest_url = crawler.settings.get('BFB2_UPLOAD_URL', None)
        if not dest_url:
            raise NotConfigured

        # instantiate the extension object
        ext = cls(dest_url, item_count)

        # connect the extension object to signals
        crawler.signals.connect(ext.spider_opened, signal=signals.spider_opened)
        crawler.signals.connect(ext.spider_closed, signal=signals.spider_closed)
        # crawler.signals.connect(ext.item_scraped, signal=signals.item_scraped)
        # return the extension object
        return ext

    def spider_opened(self, spider):
        logger.info("opened spider %s", spider.name)

    def spider_closed(self, spider):
        logger.info("closed spider %s", spider.name)
        if len(self._items) > 0:
            self._send_file(self._items)

    def item_scraped(self, item, spider):
        pass
