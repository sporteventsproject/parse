# -*- coding:utf-8 -*-

#
# © p.bormotov@brandquad.ru
#

import json
import re
import logging
import time
from copy import deepcopy

import requests
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
from scrapy.exceptions import NotConfigured



logger = logging.getLogger(__name__)

class TestBaseSpider(object):
    """
    Расширение для тестирования работы основного паука в режиме update=True (указать параметр --logfile)
        'EXTENSIONS': {
        'scrapyproject.extensions.tests.TestBaseSpider': 700,
        },

    указать в функции parse при полном сборе self.log(f"spider_items: {json.dumps(urls)}")
    """

    def __init__(self, crawler, file_format):
        self.file_format = file_format
        self.crawler = crawler

    @classmethod
    def from_crawler(cls, crawler):
        file_format = crawler.settings.get('LOG_FORMAT_FILE', 'JSON')
        ext = cls(crawler, file_format)
        crawler.signals.connect(ext.spider_opened, signal=signals.spider_opened)
        crawler.signals.connect(ext.spider_closed, signal=signals.spider_closed)
        crawler.signals.connect(ext.item_scraped, signal=signals.item_scraped)
        return ext

    def spider_opened(self, spider):
        logger.info("START TESTING %s", spider.name)
        self._items = list()
        try:
            self.logfile = spider.logger.logger.root.handlers[1].baseFilename
            self.log_path = re.sub(r'[^/]+$', '', self.logfile)
        except:
            # не запускать паука если не указан параметр --logfile
            logger.info("SPECIFY --logfile ../data/%s.log", spider.name)
            self.crawler.engine.close_spider(spider, f'SPECIFY --logfile ../data/{spider.name}.log')


    def spider_closed(self, spider):
        logger.info("END TESTING %s", spider.name)
        if len(self._items) > 0:
            if spider.update == True:
                self.search_lost_urls_true(spider)
            elif spider.update == False:
                self.search_lost_urls_false(spider)



    def item_scraped(self, item, spider):
        self._items.append(deepcopy(item))

    def search_lost_urls_true(self, spider):
        records = []
        log_file = open(self.logfile, 'r').readlines()
        item_urls = [x['url'] for x in self._items]
        lost_urls = set(spider.start_urls) - set(item_urls)
        if lost_urls:
            records = self.search_urls_from_log(lost_urls, log_file)
        if records:
            self.export_to_file(records, f'{self.log_path}{spider.name}_log')

    def search_lost_urls_false(self, spider):
        records = []
        log_file = open(self.logfile, 'r').readlines()
        item_urls = [x['url'] for x in self._items]
        # берем ссылки со страниц пагинации
        prod_urls = [json.loads(re.sub(r'^.*spider_items:', '', x)) for x in log_file if 'spider_items:' in x]
        prod_urls = [x['URL'] for xs in prod_urls for x in xs]
        lost_urls = set(prod_urls) - set(item_urls)
        if lost_urls:
            records = self.search_urls_from_log(lost_urls, log_file)
        if records:
            self.export_to_file(records, f'{self.log_path}{spider.name}_log')

    def search_urls_from_log(self, lost_urls, log_file):
        records = []
        for url in lost_urls:
            url_records = [x for x in log_file if url in x]
            records.append({'url': url, 'log_records': url_records})
        return records

    def export_to_file(self, records, fname):
        if self.file_format == 'JSON':
            with open(f'{fname}.json', 'w') as outfile:
                json.dump(records, outfile)
        else:
            pass