import json
import re

import demjson


class GetJSON(object):
    """

    Подключение к пауку:
    custom_settings = {
             'DOWNLOADER_MIDDLEWARES': {
                 'scrapyproject.extensions.mid_get_json.GetJSON': 200,
                  }
         }

     Использование в запросе:
     Request(url=url, callback=callback, meta = {'page_objects':{'xpath':list()}}
     page_objects - расширение берет данные из этого ключа.
     xpath - список xpath выражений для поиска объектов


    """
    def process_response(self, request, response, spider):
        # Функция для получения в meta, списка json объектов на странице,
        # вида : {'page_objects':{'xpath':[],'objects':[]}
        # для подключения необходимо добавить в скрипт паука следующее:
        # 1. 'scrapyproject.extensions.mid_get_json.GetJSON': 200,
        # 2. список xpath в запрос,  meta={'page_objects':{'xpath':[xp1,xp2,xp3]}}
        jo_list = []
        res_jo_list = [] # список валидных json объектов
        # получим список xpath и найдем все объекты на странице по каждому
        try:
            xp_list = request.meta['page_objects']['xpath']
        except:
            return response
        for xp in xp_list:
            s = response.xpath(xp).extract()
            jo_list.extend(s)
        # обработаем какждый найденный объект чтобы получить валидный json
        REG_01 = re.compile(r'var\s+\w+\s*=\s*(\{.*)', re.DOTALL)
        REG_02 = re.compile(r'JSON.stringify\((\{.*)', re.DOTALL)
        REG_03 = re.compile(r'var\s+\w+\s*=\s*(\[.*)', re.DOTALL)
        REG_04 = re.compile(r'\s*\b\w+\b\s*=\s*(\{.*)', re.DOTALL)
        REG_05 = re.compile(r'\s+var\s+.+\w\s+=\s+.+?(\()', re.DOTALL)
        # REG_06 = re.compile(r's*\b\w+\b\s*=\s*[\(\{\[].*?[\}\)\]]]', re.DOTALL|re.UNICODE)
        for j in jo_list:
            try:
                jo = json.loads(j)
                res_jo_list.append(jo)
                continue
            except ValueError:
                pass
            try:
                jo = self.json_verify(REG_01,j)
                res_jo_list.append(jo)
                continue
            except ValueError:
                pass
            try:
                jo = self.json_verify(REG_02,j)
                res_jo_list.append(jo)
                continue
            except ValueError:
                pass
            try:
                jo = self.json_verify(REG_03,j)
                res_jo_list.append(jo)
                continue
            except ValueError:
                pass
            try:
                jo = self.json_verify(REG_04,j)
                res_jo_list.append(jo)
                continue
            except ValueError:
                pass

            try:
                jo = self.json_verify(REG_05, j)
                res_jo_list.append(jo)
                continue
            except ValueError:
                pass
            try:
                jo = demjson.decode(j)
                res_jo_list.append(jo)
                continue
            except demjson.JSONDecodeError:
                pass
            # try:
            #     jo = self.json_verify(REG_06, j)
            #     res_jo_list.append(jo)
            #     continue
            # except ValueError:
            #     pass
        request.meta.update({'page_objects': {'xpath': xp_list,
                                              'objects': res_jo_list}})
        return response

    def json_verify(self, REG, j):
        jo = re.search(REG, j)
        if jo:
            jo = jo.group(1)
            res = []
            start_point = end_point = 0
            for idx, x in enumerate(jo):
                if x == '{' or x == '[':
                    res.append(idx)
                elif x == '}' or x == ']':
                    start_point = res.pop(-1)
                    if not res:
                        end_point = idx + 1
                        break
            return json.loads(jo[start_point:end_point])
        raise ValueError
