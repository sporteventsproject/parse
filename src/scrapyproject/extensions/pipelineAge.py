# -*- coding:utf-8 -*-

import re

from ..spiders.helpers.jtree import JDict

PTRN = (re.compile(r'(с|от|до)(\s+|:)\d+\s*(л|м)е(т|с)', re.IGNORECASE | re.MULTILINE),
        re.compile(r'(стар|млад)ше\s+\d+.+?(л|м)е(т|с)', re.IGNORECASE| re.MULTILINE),
        re.compile(r'\bне\s+подходит\s+\W+(стар|млад)ше\s+\d+.+?(л|м)е(т|с)', re.IGNORECASE|re.MULTILINE),)


class AgeExtractor(object):

    def __init__(self,  fields = ['__description'], key='ВОЗРАСТ'):

        def jn(*a):
            return " ".join(a)

        self.fields = fields
        self.key = key
        self.f = jn

    @classmethod
    def from_crawler(cls, crawler):
        return cls(fields = crawler.settings.get('PIPELINE_AGE_FIELDS', ['__description']),
                   key = crawler.settings.get('PIPELINE_AGE_KEY', 'ВОЗРАСТ'))

    def process_item(self, item, spider):
        jd = JDict(item)
        l = self.f(*list(map(lambda x: self.f(*jd.key(x)), self.fields )))
        l = re.sub(r'&\D+?;', ' ', l)
        d = list(map(lambda z: z.group(),
            filter(lambda y: y, map(lambda x: x.search(l), PTRN))))
        if d:
            item['metadata'][self.key] = " ".join(d)
        return item


