# -*- coding:utf-8 -*-

#
#  p.bormotov@brandquad.ru
#
import logging

logger = logging.getLogger(__name__)


class ProxySingle(object):
    """
        custom_settings = {
        'DOWNLOADER_MIDDLEWARES': {
            'scrapyproject.extensions.proxy_single.ProxySingle': 50,
        },
        'PROXY_S': '36.67.198.35:3128',
        'PROXY_S_LOGIN': 'xxxxxx',
        'PROXY_S_PASSWD': 'xxxxxx',
        'PROXY_S_SCHEME': 'http',
    }

    """

    def __init__(self, proxy=None, login=None, passwd=None, scheme='http'):
        self.proxy = proxy
        self.login = login
        self.passwd = passwd
        self.scheme = scheme

    @classmethod
    def from_crawler(cls, crawler):
        logger.info("Start create ProxyExtension for crawler")
        proxy = crawler.settings.get('PROXY_S', None)
        login = crawler.settings.get('PROXY_S_LOGIN', None)
        passwd = crawler.settings.get('PROXY_S_PASSWD', None)
        scheme = crawler.settings.get('PROXY_S_SCHEME', 'https')
        spider = cls(proxy=proxy, login=login, passwd=passwd, scheme=scheme)
        return spider

    def process_request(self, request, spider):
        if self.login and self.passwd:
            request.meta['proxy'] = f"{self.scheme}://{self.login}:{self.passwd}@{self.proxy}"
        else:
            request.meta['proxy'] = f"{self.scheme}://{self.proxy}"
        return

