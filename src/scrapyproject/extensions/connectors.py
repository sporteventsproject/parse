# -*- coding:utf-8 -*-

# if you do not know or do not understand how it works - do not go
# © s.deynego@brandquad.ru
#

from scrapy import signals
from scrapy.exceptions import NotConfigured


class BQProxyConnector(object):

    def __init__(self):
        pass

    @classmethod
    def from_crawler(cls, crawler):
        # first check if the extension should be enabled and raise
        # NotConfigured otherwise
        if not crawler.settings.getbool('BQPROXY_ENABLED'):
            raise NotConfigured

        # get the number of items from settings
        item_count = crawler.settings.getint('MYEXT_ITEMCOUNT', 1000)

        # instantiate the extension object
        ext = cls(item_count)

        # connect the extension object to signals
        crawler.signals.connect(ext.spider_opened, signal=signals.spider_opened)
        crawler.signals.connect(ext.spider_closed, signal=signals.spider_closed)
        crawler.signals.connect(ext.item_scraped, signal=signals.item_scraped)
        crawler.signals.connect(ext.request_process, signal=signals.request_scheduled)
        # return the extension object
        return ext

    def process_request(self):
        pass

    def process_response(self):
        pass
