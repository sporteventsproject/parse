# -*- coding:utf-8 -*-
#

from copy import deepcopy  # <-Это нахрена?

from scrapy.exceptions import NotConfigured


class MetadataKeyUpdater(object):
    METADATA_KEYS = [
        ('ЭФФЕКТ', ['ХАРАКТЕРИСТИКА ПРИМЕНЕНИЯ', 'ДЕЙСТВИЕ', 'РЕЗУЛЬТАТ']),
        ('ОБЪЕМ', ['ОБЪЕМ, В МИЛЛИТРАХ', 'ОБЪЕМ ТОВАРА', 'ОБЪЕМ, В МИЛЛИЛИТРАХ']),
    ]

    def process_item(self, item, spider):
        """
        :param item:
        :param spider:
        :return:
        """
        l = list(filter(lambda x: x in spider.name, ['_search' '_category' '_reviews']))
        if not l:
            _metadata = item.get('metadata', {})
            new_metadata = deepcopy(_metadata)
            for i in _metadata:
                new_key = [x for x in self.METADATA_KEYS if i in x[1]]
                if new_key:
                    new_metadata[new_key[0][0]] = new_metadata.pop(i)
            item['metadata'] = new_metadata
        return item


class MetadataKeysUpdUniversal(object):
    """
    Заменяет ключи из списка на новый ключ в объектк metadate

    'KEYS_MAP': [
        ('KEY', ('OLD_KEY_1', 'OLD_KEY_2'),),
        ('KEY2', ('OLD_KEY_3', 'OLD_KEY_4'),)
        ],
    {
        "scrapyproject.extensions.mapper.MetadataKeysUpdUniversal": 800
    }
    """
    def __init__(self, crawler=None):
        self._map = crawler.settings.get('KEYS_MAP', None)
        if not self._map:
            raise NotConfigured()

    @classmethod
    def from_crawler(cls, crawler):
        return cls(crawler=crawler)

    def process_item(self, item, spider):
        m = item.get('metadata', None)
        if m:
            current_items = list(map(lambda x: x, list(m.items())))
            r =list(filter(lambda k: len(k) > 0,
                [list(filter(lambda z: z,
                    map(lambda k: (k, x[0]) if k in x[1] else None, m))) for x in self._map]))
            rl = list()
            for x in r:
                if isinstance(x, list) and len(x)>0:
                    rl.extend(x)
                elif isinstance(x, tuple):
                    rl.append(x)
            nm = {}
            nm.update(rl)
            r = list(map(lambda x: (nm.get(x[0], x[0]), x[1]), current_items))
            item['metadata'] = {}
            item['metadata'].update(r)
        return item
