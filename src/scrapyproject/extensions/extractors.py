# -*- coding:utf-8 -*-

# if you do not know or do not understand how it works - do not go
# © s.deynego@brandquad.ru
#

import json
import re

import demjson
from lxml import etree
from scrapy import Selector

#
#


KEY = 'page_objects'
BLANK_RE = type(re.compile(""))


def _clr_cs(s):
    return re.sub(r'[\x00-\x1f\x7f-\x9f]', '', s)


def _clr_cm(s):
    return re.sub(r'<!--.+?(-->)', '', s)

class JsonExtractor(object):
    """
    Плагин предназначен для получения на лету данных из загруженной стриницы извлекаемых через xpath
    и обрабатываемых регулярными выражениями(или без них)
    Подключение к пауку:

    custom_settings = {
             'DOWNLOADER_MIDDLEWARES': {
                 'scrapyproject.extensions.extractors.JsonExtractor': 200,
                  }
         }

     Использование в запросе:
     Request(url=url, callback=callback, meta = {'page_objects':{'json':{'xpath':list, 'regexp':[]}}}
     page_objects - словарь, ключи которого содержат различные типы извлекаемых данных
    'default':{'xpath':[], 'regexp':[]}
    'json':{'xpath':[], 'regexp':[]}
    'links':{'xpath':[], 'regexp':[]}
    'img':{'xpath':[], 'regexp':[]}
    'texts':{'xpath':[], 'regexp':[]}
    и другие

    результаты для каждого ключа помещаются в поле result:[]
    """

    def process_response(self, request, response, spider):

        spider.logger.info("processing response: {}".format(response.url))
        if response.status in [200]:

            def _re_apply(regexp, src):
                try:
                    if isinstance(regexp, (str, BLANK_RE)):
                        try:
                            _reo = re.compile(regexp)
                        except:
                            _reo = regexp
                    return _reo.search(src).group()
                except Exception as e:
                    spider.logger.error("Error regexp apply url:{}".format(response.url))
                    return None

            def _json_ext(v):
                try:
                    return json.loads(v)
                except json.JSONDecodeError:
                    try:
                        _rt = etree.parse(response.text)
                        _s = _rt.xpath("//script[contains(text(), 'var page_data')]/text()")
                        return demjson.decode(v)
                    except Exception as ex:
                        spider.logger.error("Error json decode url:{} ".format(response.url))
                        return []

            def _extract(x):
                if isinstance(x, (tuple,)):
                    main_key = x[0]
                    d_dict = x[1]
                    xp_result = []
                    # sel = Selector(text=_clr_cm(_clr_cs(response.text)))
                    list(map(lambda m: xp_result.extend(m),
                             list(map(lambda item:
                                      list(filter(lambda item1: item1,
                                                  response.xpath(item).extract())),
                                      d_dict.get('xpath', [])))))
                    xp_result = list(set(xp_result))
                    o_result = []
                    list(map(lambda l: o_result.extend(l),
                             list(map(lambda s:
                                      list(map(lambda r:
                                               _re_apply(r, s),
                                               d_dict.get('regexp', []))),
                                      xp_result))))
                    if main_key.upper() in 'JSON':
                        o_result = list(map(_json_ext, o_result))
                    if main_key.upper() in 'LINKS':
                        pass
                    if main_key.upper() in 'DEFAULT':
                        pass
                    d_dict['result'] = list(map(lambda y: y, o_result))
                    return (main_key, d_dict)
                return x

            if KEY in list(request.meta.keys()):
                items_dict = request.meta[KEY]
                items = list(map(_extract, list(items_dict.items())))

                items_dict.update(items)
        return response


JSV_KEY = "jscript_obj"


class JScriptExtractor(object):
    """
    Плагин предназначен для получения на лету json данных из загруженной стриницы извлекаемых из <script>....</script>
    Подключение к пауку:

    custom_settings = {
             'DOWNLOADER_MIDDLEWARES': {
                 'scrapyproject.extensions.extractors.JScriptExtractor': 200,
                  }
         }

     Использование в запросе:
     Request(url=url, callback=callback, meta = {'jscript_obj':{'regexp': '', 'name':'var page_data'}}}
     jscript_obj - словарь,  c параметрами
    результаты для каждого ключа помещаются в поле result:[]
    """

    def process_response(self, request, response, spider):
        def _json(x):
            try:
                return json.loads(x)
            except json.JSONDecodeError as err:
                try:
                    spider.logger.error("json decode error: {}".format(response.url))
                    return demjson.decode(x)
                except Exception as err:
                    spider.logger.error("demjson decode error: {}".format(response.url))
                    return None

        if JSV_KEY in list(request.meta.keys()):
            items_dict = request.meta[JSV_KEY]
            items = re.finditer(r'<script.+?(</script>)', response.text, re.MULTILINE | re.IGNORECASE | re.UNICODE)
            items = list(filter(lambda y: items_dict.get('name', 0) in y, list(map(lambda x: x.group(), items))))
            items = list(filter(lambda y: y,
                                list(map(lambda x: re.search(items_dict.get('regexp', None), x,
                                                             re.IGNORECASE | re.MULTILINE | re.UNICODE).group(),
                                         items))))
            items = list(filter(lambda y: y, list(map(lambda x: _json(x), items))))
            items_dict.update([('result', items,)])
        return response



XP_KEY = 'xpath_obj'


class XPathExtractor(object):
    """
    Плагин предназначен для получения на лету данных из загруженной стриницы извлекаемых через xpath
    и обрабатываемых регулярными выражениями(или без них)
    Подключение к пауку:

    custom_settings = {
             'DOWNLOADER_MIDDLEWARES': {
                 'scrapyproject.extensions.extractors.XPathExtractor': 201,
                  }
         }

     Использование в запросе:
     Request(url=url, callback=callback, meta = {'page_objects':{'xpath_obj':
        {'xpath_value_1': {'exp': list(), 'regexp': list()},
         'xpath_value_2': {'exp': list(), 'regexp': list()} }
    }
    и другие

    результаты для каждого ключа помещаются в поле 'result': list()
    """

    def process_response(self, request, response, spider):

        def _extract(selector, xpath_item):
            a = list()
            [a.extend(selector.xpath(x).extract()) for x in xpath_item[1].get('exp', [])]
            a = list(map(lambda x: x.rstrip().lstrip(), a))
            r = list(filter(lambda x: x, a))
            return (xpath_item[0],
                    {'exp': xpath_item[1].get('exp', []),
                     'regexp': xpath_item[1].get('regexp', []),
                     'result': r},)

        if XP_KEY in list(request.meta.keys()):
            spider.logger.info("processing response: {}".format(response.url))
            html = re.sub(r'[\x00-\x1F\x7F]', '', response.text)
            html = Selector(text=html).xpath('//html')
            items_dict = request.meta[XP_KEY]
            items = [_extract(html, item) for item in
                     list(items_dict.items())]  # list(map(_extract, list(items_dict.items())))
            items_dict.update(items)
        return response

#
# class XPathMapExtractor(object):
#     """
# Плагин предназначен для получения на лету данных из загруженной стриницы извлекаемых через xpath
#     и обрабатываемых регулярными выражениями(или без них)
#     Подключение к пауку:
#
#     custom_settings = {
#              'DOWNLOADER_MIDDLEWARES': {
#                  'scrapyproject.extensions.extractors.XPathExtractor': 201,
#                   }
#          }
#
#      Использование в запросе:
#      Request(url=url, callback=callback, meta = {'page_objects':{'xpath_obj':
#         {'xpath_value_1': {'key': 'key_val_1,'exp': list(), 'regexp': list()},
#          'xpath_value_2': {'key': key_val_2,'exp': list(), 'regexp': list()} }
#     }
#     и другие
#
#     результаты для каждого ключа помещаются в поле 'result': list()
#     """
#
#     def process_response(self, request, response, spider):
#         def _extract(selector, xpath_item):
#             a = list()
#             [a.extend(selector.xpath(x).extract()) for x in xpath_item[1].get('exp', [])]
#             a = list(map(lambda x: x.rstrip().lstrip(), a))
#             r = list(filter(lambda x: x, a))
#             return (xpath_item[0],
#                     {'key': xpath_item[1].get('key', ),
#                     'exp': xpath_item[1].get('exp', []),
#                      'regexp': xpath_item[1].get('regexp', []),
#                      'result': r},)
#
#         if XP_KEY in list(request.meta.keys()):
#             spider.logger.info("processing response: {}".format(response.url))
#             html = re.sub(r'[\x00-\x1F\x7F]', '', response.text)
#             html = Selector(text=html).xpath('//html')
#             items_dict = request.meta[XP_KEY]
#             items = [_extract(html, item) for item in
#                      list(items_dict.items())]  # list(map(_extract, list(items_dict.items())))
#             items_dict.update(items)
#         return response
