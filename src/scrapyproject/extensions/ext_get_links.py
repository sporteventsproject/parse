# -*- coding:utf-8 -*-

import logging

from scrapy import signals
from scrapy.exceptions import NotConfigured

logger = logging.getLogger(__name__)


class GetStartLinksBFB(object):
    """
    Расширение для загрузки стартовых ссылок с сервера BFB

    custom_settings = {
        'BFB2_GET_LINKS': True, # получать стартовые ссылки с сервера
        EXTENSIONS = {
            'scrapyproject.extensions.ext_get_links .GetStartLinksBFB': 501,
        }
    }
    """

    def __init__(self):
        pass

    @classmethod
    def from_crawler(cls, crawler):
        if not crawler.settings.getbool('BFB2_GET_LINKS'):
            raise NotConfigured
        ext = cls()
        crawler.signals.connect(ext.spider_opened, signal=signals.spider_opened)
        return ext

    def spider_opened(self, spider):
        """
        Получить стартовые урл с сервера.
        :param spider:
        :return:
        """
        pass
