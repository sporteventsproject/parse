# -*- coding:utf-8 -*-


# if you do not know or do not understand how it works - do not go
# © s.deynego@brandquad.ru
#
from scrapy import signals


class BQProxyMiddleware(object):
    _defaults = [('PEER', 'scrapping2.westeurope.cloudapp.azure.com'),  # proxy host
                 ('PORT', ''),  # proxy port
                 ('PASS', ''),  # encription key
                 ]
    _settings = []
    _headers = ('X-BQ-SRC', 'X-BQ-KEY', 'X-BQ-DATA', 'X-BQ-RES')

    def __init__(self):
        pass

    @classmethod
    def from_crawler(cls, crawler):
        o = cls(crawler)
        crawler.signals.connect(o.open_spider, signals.spider_opened)
        return o

    def open_spider(self, spider):
        pass

    def spider_close(self, spider):
        pass

    def process_request(self, request, spider):
        pass

    def process_response(self, request, response, spider):
        pass

    def process_exception(self, request, exception, spider):
        pass

    def _encode(self, o):
        return o

    def _decode(self, o):
        return o
