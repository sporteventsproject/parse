import asyncio
from aiohttp import web
import re
import os
import time

TOKEN = os.getenv('APP_TOKEN', '222')

async def run_spider(request):
    await check_token(request)
    jo = await request.json()
    cmd = f"cd /code/src && scrapy crawl {jo['spider']} " \
        f"-o ./data/data/{jo['file_name']}.json " \
        f"--logfile=./data/log/{jo['file_name']}.log"
    if jo['update'] is not None:
        cmd = cmd + f" -a update={jo['update']}"
    if jo['start_urls']:
        cmd = cmd + f" -a start_urls={jo['start_urls']}"
    if jo['accounts']:
        cmd = cmd + f" -a accounts={jo['accounts']}"
    if jo['parts']:
        cmd = cmd + f" -a part={jo['part']} -a parts={jo['parts']} -a file_name={jo['file_name']}"
    if jo['file_name']:
        cmd = cmd + f" -a file_name={jo['file_name']}"

    # local
    # main_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
    # cmd = cmd.replace('/code/src', main_dir)

    pid = await run(cmd)
    time.sleep(5)
    ppid = await run_with_stdout(f'ps --ppid {pid} -o pid')
    ppid = re.sub(r'\D', '', ppid)
    jo.update({'pid': pid, 'ppid': ppid})
    return web.json_response(jo)


async def stop_spiders(request):
    await check_token(request)
    pids = await request.json()
    print(pids)
    pids = [str(x) for x in pids]
    cmd = f'kill -s SIGALRM {" ".join(pids)}'
    print(cmd)
    await run(cmd)
    return web.json_response(pids)


async def get_process_list(request):
    await check_token(request)
    cmd = 'ps aux | grep crawl'
    header = ['USER', 'PID', '%CPU', '%MEM', 'VSZ', 'RSS', 'TTY', 'STAT', 'START', 'TIME', 'COMMAND']
    pr_list = await run_with_stdout(cmd)
    pr_list = pr_list.split('\n')
    pr_list = [re.sub('\s+', ' ', x) for x in pr_list if 'scrapy' in x]
    pr_list = [x.split(' ') for x in pr_list if x]
    res = []
    for x in pr_list:
        pr = ' '.join(x[10:])
        x[10] = pr
        x = x[:11]
        res.append(dict(zip(header, x)))
    return web.json_response(res)

async def get_spiders_list(request):
    await check_token(request)
    cmd = 'cd /code/src && scrapy list'

    # local
    # main_dir = os.path.abspath(os.path.join(os.getcwd(), os.pardir))
    # cmd = cmd.replace('/code/src', main_dir)

    res = await run_with_stdout(cmd)
    res = res.split('\n')
    return web.json_response(res)


async def run(cmd):
    proc = await asyncio.create_subprocess_shell(
        cmd,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE)
    return proc.pid


async def run_with_stdout(cmd):
    proc = await asyncio.create_subprocess_shell(
        cmd,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE)
    stdout, stderr = await proc.communicate()
    print(f'[{cmd!r} exited with {proc.returncode}]')
    if stdout:
        print(f'[stdout]\n{stdout.decode()}')
        return stdout.decode()
    if stderr:
        print(f'[stderr]\n{stderr.decode()}')
        return stderr.decode()


async def check_token(request):
    x = request.headers.get('token', '')
    if x == TOKEN:
        return
    else:
        raise web.HTTPForbidden()

async def main_app():
    app = web.Application()
    app.router.add_post('/runspider', run_spider)
    app.router.add_post('/stopspiders', stop_spiders)
    app.router.add_post('/getprocesslist', get_process_list)
    app.router.add_post('/getspiderslist', get_spiders_list)
    return app
    # web.run_app(app, port=5858)